<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 01/03/2017, 21:31
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file example.php
 */

require __DIR__ . '/vendor/autoload.php';
date_default_timezone_set('UTC');

use SportMonks\SoccerApi;

$sportMonks = new SoccerApi([
    'base_uri'  =>  'https://soccer.sportmonks.com/api/v2.0/',
    'query'     => [
        'api_token' => '*******************************'
    ],
]);

foreach ($sportMonks->leagues()->all() as $league) {
    echo $league->getName().PHP_EOL;
}