<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 28/02/2017, 21:27
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Client.php
 */

namespace SportMonks;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use SportMonks\Middleware\RetryHandler;
use stdClass;

/**
 * Class Client
 * @package SportMonks
 */
class Client extends HttpClient
{
    /**
     * @var integer
     */
    private $maxRequestsPerHour;

    /**
     * @var array
     */
    private $include;

    /**
     * @var integer
     */
    private $perPage;

    /**
     * SportMonks Soccer API Client constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->include = [];
        $this->perPage = 150;
        $this->maxRequestsPerHour = 2000;
        $handler = HandlerStack::create();
        $handler->push(new RetryHandler([
            'retry_if' => function ($retries, $request, $response, $e) {
                return $e instanceof RequestException && strpos($e->getMessage(), 'ssl') !== false;
            }
        ]), 'retry_handler');


        parent::__construct(array_merge($config, compact('handler')));
    }

    /**
     * Makes a GET request using the object HTTP Client.
     * @param string $path
     * @return stdClass|stdClass[]
     */
    protected function get(string $path)
    {
        $page = 0;
        $result = [];
        do {
            $page++;
            $query = $this->getConfig('query') + [
                    'include' => implode(',', $this->include),
                    'per_page' => $this->perPage,
                    'page' => $page
                ];

            $response = parent::get($path, compact('query'))->getBody();

            // Sleep after each request to prevent reaching the
            // maximum allowed amount of request per minute.
            if (!empty($this->maxRequestsPerHour)) {
                usleep(3600 / $this->maxRequestsPerHour * 1000000);
            }

            $response = json_decode($response);

            if (isset($response->error)) {
                return [];
            }

            $res = $response->data ?? $response;
            $result = is_array($res) ? array_merge($result, $res) : $res;

        } while (isset($response->meta->pagination) && $page < $response->meta->pagination->total_pages);

        return $result;
    }

    /**
     * Add the relation to include in the response
     *
     * @param string $relation
     * @return static
     */
    public function including(string $relation)
    {
        $this->include[] = $relation;

        return $this;
    }
}
