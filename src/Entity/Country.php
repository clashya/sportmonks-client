<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 05/09/2016, 22:58
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Country.php
 */

namespace SportMonks\Entity;

/**
 * Class Country
 * @package SportMonks\Entities
 */
class Country
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $isoCode;

    /**
     * @var string
     */
    private $flag;

    /**
     * League constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Country
     */
    public function setName($name): Country
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsoCode(): ?string
    {
        return $this->isoCode;
    }

    /**
     * @param string $isoCode
     * @return Country
     */
    public function setIsoCode($isoCode): Country
    {
        $this->isoCode = $isoCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFlag(): ?string
    {
        return $this->flag;
    }

    /**
     * @param string $flag
     * @return Country
     */
    public function setFlag($flag): Country
    {
        $this->flag = $flag;
        return $this;
    }

    /**
     * @param integer $id
     * @param \stdClass $countryJson
     * @return Country
     */
    public static function fromJson($id, $countryJson): Country
    {
        $league = new self($id);

        $league->setName($countryJson->name)
            ->setIsoCode($countryJson->extra?->iso)
            ->setFlag($countryJson->image_path);

        return $league;
    }
}
