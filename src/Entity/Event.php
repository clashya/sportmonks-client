<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 28/02/2016, 23:03
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Event.php
 */

namespace SportMonks\Entity;

/**
 * Class Event
 * @package SportMonks\Entities
 */

use SportMonks\Entity\Event\Card;
use SportMonks\Entity\Event\Goal;
use SportMonks\Entity\Event\Penalty;
use SportMonks\Entity\Event\Substitution;

/**
 * Class Event
 * @package SportMonks\Entities
 */
class Event
{
    /**
     * @var array
     */
    protected static $uniqueAttributes = ['fixtureId', 'teamId', 'minute', 'minuteExtra'];
    /**
     * @var integer
     */
    private $id;
    /**
     * @var integer
     */
    private $fixtureId;
    /**
     * @var integer
     */
    private $teamId;
    /**
     * @var integer
     */
    private $minute;
    /**
     * @var integer
     */
    private $minuteExtra;

    /**
     * Fixture constructor.
     * @param integer $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getFixtureId(): int
    {
        return $this->fixtureId;
    }

    /**
     * @param integer $fixtureId
     * @return Event
     */
    public function setFixtureId($fixtureId): Event
    {
        $this->fixtureId = $fixtureId;

        return $this;
    }

    /**
     * @return integer
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * @param integer $teamId
     * @return Event
     */
    public function setTeamId($teamId): Event
    {
        $this->teamId = $teamId;

        return $this;
    }

    /**
     * @return integer
     */
    public function getMinute(): int
    {
        return $this->minute;
    }

    /**
     * @param integer $minute
     * @return Event
     */
    public function setMinute($minute): Event
    {
        $this->minute = $minute;

        return $this;
    }

    /**
     * @return integer
     */
    public function getMinuteExtra(): ?int
    {
        return $this->minuteExtra;
    }

    /**
     * @param integer $minuteExtra
     * @return Event
     */
    public function setMinuteExtra($minuteExtra): Event
    {
        $this->minuteExtra = $minuteExtra;

        return $this;
    }

    /**
     * Returns the instance's unique key.
     * @return string
     */
    public function getUniqueKey(): string
    {
        $namespaceParts = explode('\\', \get_class($this));
        return array_reduce(static::$uniqueAttributes, function ($carry, $uniqueAttr) {
                return $carry . $this->{$uniqueAttr};
            }) . end($namespaceParts);
    }

    /**
     * @param integer $id
     * @param \stdClass $eventObj
     * @return Event
     */
    public static function fromJson($id, $eventObj): ?Event
    {
        /** @var Event $event */
        $event = null;
        $type = $eventObj->type;

        switch ($type) {
            case 'goal':
                /** @var Goal $event */
                $event = (new Goal($id))
                    ->setPlayerId($eventObj->player_id);
                if (!empty($eventObj->related_player_id)) {
                    $event->setAssistPlayerId($eventObj->related_player_id);
                }
                break;
            case 'own-goal':
                /** @var Goal $event */
                $event = (new Goal($id))
                    ->setOwnGoal(true)
                    ->setPlayerId($eventObj->player_id);
                if (!empty($eventObj->related_player_id)) {
                    $event->setAssistPlayerId($eventObj->related_player_id);
                }
                break;
            case 'var':
                if ($eventObj->var_result == 'Goal Disallowed') {
                    /** @var Goal $event */
                    $event = (new Goal($id))
                        ->setDisallowed(true)
                        ->setPlayerId($eventObj->player_id);
                    if (!empty($eventObj->related_player_id)) {
                        $event->setAssistPlayerId($eventObj->related_player_id);
                    }
                } else return null;
                break;
            case 'penalty':
                /** @var Penalty $event */
                $event = (new Penalty($id))
                    ->setPlayerId($eventObj->player_id);
                break;

            case 'missed_penalty':
                /** @var Penalty $event */
                $event = (new Penalty($id))
                    ->setPlayerId($eventObj->player_id)
                    ->setMissed(true);
                break;
            case 'pen_shootout_goal':
                /** @var Penalty $event */
                $event = (new Penalty($id))
                    ->setPlayerId($eventObj->player_id)
                    ->setShootOut(true);
                break;
            case 'pen_shootout_miss':
                /** @var Penalty $event */
                $event = (new Penalty($id))
                    ->setPlayerId($eventObj->player_id)
                    ->setShootOut(true)
                    ->setMissed(true);
                break;
            case 'yellowcard':
                /** @var Card $event */
                $event = (new Card($id, Card::$YELLOW))
                    ->setPlayerId($eventObj->player_id);
                break;

            case 'yellowred':
                /** @var Card $event */
                $event = (new Card($id, Card::$YELLOW_RED))
                    ->setPlayerId($eventObj->player_id);
                break;

            case 'redcard':
                /** @var Card $event */
                $event = (new Card($id, Card::$RED))
                    ->setPlayerId($eventObj->player_id);
                break;

            case 'substitution':
                /** @var Substitution $event */
                $event = (new Substitution($id))
                    ->setPlayerInId($eventObj->player_id)
                    ->setPlayerOutId($eventObj->related_player_id);
                break;
            default:
                $event = new self($id);
        }

        return $event
            ->setFixtureId($eventObj->fixture_id)
            ->setMinute($eventObj->minute)
            ->setMinuteExtra($eventObj->extra_minute)
            ->setTeamId($eventObj->team_id);
    }
}
