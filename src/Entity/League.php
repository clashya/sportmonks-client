<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 05/09/2016, 22:58
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file League */

namespace SportMonks\Entity;

/**
 * Class League
 * @package SportMonks\Entities
 */
class League
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $countryId;

    /**
     * @var Season
     */
    private $season;

    /**
     * @var string
     */
    private $logo;

    /**
     * League constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return League
     */
    public function setName($name): League
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     * @return League
     */
    public function setCountryId($countryId): League
    {
        $this->countryId = $countryId;
        return $this;
    }

    /**
     * @return Season
     */
    public function getSeason(): Season
    {
        return $this->season;
    }

    /**
     * @param Season $season
     * @return League
     */
    public function setSeason($season): League
    {
        $this->season = $season;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     * @return League
     */
    public function setLogo($logo): League
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @param integer $id
     * @param \stdClass $competitionJson
     * @return League
     */
    public static function fromJson($id, $competitionJson): League
    {
        $league = new self($id);

        $league->setName($competitionJson->name)
            ->setCountryId($competitionJson->country_id)
            ->setLogo($competitionJson->logo_path);

        if (!empty($competitionJson->season)) {
            $season = Season::fromJson($competitionJson->season->data->id, $competitionJson->season->data);
            $league->setSeason($season);
        }

        return $league;
    }
}
