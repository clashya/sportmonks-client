<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 29/08/2016, 20:11
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Team.php
 */

namespace SportMonks\Entity;

/**
 * Class Team
 * @package SportMonks\Entities
 */
class Team
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $logo;

    /**
     * Team constructor.
     * @param integer $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Team
     */
    public function setName($name): Team
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     * @return Team
     */
    public function setLogo($logo): Team
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @param integer $id
     * @param \stdClass $teamJson
     * @return Team
     */
    public static function fromJson($id, $teamJson): Team
    {
        $team = new self($id);

        $team->setName($teamJson->name)
            ->setLogo($teamJson->logo_path);

        return $team;
    }

}