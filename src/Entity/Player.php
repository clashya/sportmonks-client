<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 29/08/2016, 20:18
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Player.php
 */

namespace SportMonks\Entity;


/**
 * Class Player
 * @package SportMonks\Entities
 */

use Carbon\Carbon;

/**
 * Class Player
 * @package SportMonks\Entities
 */
class Player
{
    public static $UNKNOWN = [0, 1];

    private static $POSITIONS = [
        1 => 'Goalkeeper',
        2 => 'Defender',
        3 => 'Midfielder',
        4 => 'Attacker',
    ];

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $countryId;

    /**
     * @var string
     */
    private $position;

    /**
     * @var Carbon
     */
    private $dateOfBirth;

    /**
     * @var integer
     */
    private $shirtNumber;

    /**
     * @var string
     */
    private $imagePath;

    /**
     * Player constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Player
     */
    public function setName($name): Player
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPosition(): ?string
    {
        return $this->position;
    }

    /**
     * @param string $position
     * @return Player
     */
    public function setPosition($position): Player
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Carbon
     */
    public function getDateOfBirth(): ?Carbon
    {
        return $this->dateOfBirth;
    }

    /**
     * @param Carbon $dateOfBirth
     * @return Player
     */
    public function setDateOfBirth(Carbon $dateOfBirth): Player
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountryId(): ?int
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     * @return Player
     */
    public function setCountryId($countryId): Player
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * @return int
     */
    public function getShirtNumber(): ?int
    {
        return $this->shirtNumber;
    }

    /**
     * @param int $shirtNumber
     * @return Player
     */
    public function setShirtNumber($shirtNumber): Player
    {
        $this->shirtNumber = $shirtNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     * @return Player
     */
    public function setImagePath($imagePath): Player
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * @param $id
     * @param \stdClass $playerObj
     * @return Player
     * @throws \InvalidArgumentException
     */
    public static function fromJson($id, $playerObj): Player
    {
        $player = new self($id);

        if (!empty($playerObj->birthdate)) {
            $dateOfBirth = Carbon::createFromFormat('d/m/Y', $playerObj->birthdate);
            $player->setDateOfBirth($dateOfBirth);
        }

        if (!empty($playerObj->position_id && key_exists($playerObj->position_id, self::$POSITIONS))) {
            $player->setPosition(self::$POSITIONS[$playerObj->position_id]);
        }

        $playerName = $playerObj->display_name;
        if (empty($playerName)) {
            $playerName = $playerObj->common_name;
        }
        if (empty($playerName)) {
            $playerName = $playerObj->fullname;
        }

        $player->setName($playerName)
            ->setShirtNumber($playerObj->shirt_number ?? null)
            ->setCountryId($playerObj->country_id)
            ->setImagePath($playerObj->image_path);

        return $player;
    }
}
