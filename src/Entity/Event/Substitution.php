<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 15/03/2017, 20:22
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Substitution.php
 */

namespace SportMonks\Entity\Event;

use SportMonks\Entity\Event;

/**
 * Class Substitution
 * @package SportMonks\Entities\Events
 */
class Substitution extends Event
{
    /**
     * @var array
     */
    protected static $uniqueAttributes = ['fixtureId', 'teamId', 'playerInId'];

    /**
     * @var integer
     */
    protected $playerInId;

    /**
     * @var integer
     */
    protected $playerOutId;

    /**
     * @return int
     */
    public function getPlayerInId(): ?int
    {
        return $this->playerInId;
    }

    /**
     * @param int $playerInId
     * @return Substitution
     */
    public function setPlayerInId($playerInId): Substitution
    {
        $this->playerInId = $playerInId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPlayerOutId(): ?int
    {
        return $this->playerOutId;
    }

    /**
     * @param int $playerOutId
     * @return Substitution
     */
    public function setPlayerOutId($playerOutId): Substitution
    {
        $this->playerOutId = $playerOutId;

        return $this;
    }
}