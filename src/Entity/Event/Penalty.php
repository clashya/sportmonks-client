<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 15/03/2017, 20:18
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Penalty.php
 */

namespace SportMonks\Entity\Event;

use SportMonks\Entity\Event;
use SportMonks\Entity\Player;

/**
 * Class Penalty
 * @package SportMonks\Entities\Events
 */
class Penalty extends Event
{
    /**
     * @var array
     */
    protected static $uniqueAttributes = ['fixtureId', 'teamId', 'minute', 'minuteExtra', 'playerId'];

    /**
     * @var integer
     */
    protected $playerId;

    /**
     * @var bool
     */
    protected $wasMissed;

    /**
     * @var bool
     */
    protected $isShootOut;

    /**
     * Penalty constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        parent::__construct($id);
        $this->wasMissed = false;
        $this->isShootOut = false;
    }

    /**
     * @return int
     */
    public function getPlayerId(): ?int
    {
        return $this->playerId;
    }

    /**
     * @param int $playerId
     * @return Penalty
     */
    public function setPlayerId($playerId): Penalty
    {
        if (!\in_array($playerId, Player::$UNKNOWN, false)) {
            $this->playerId = $playerId;
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function wasMissed(): bool
    {
        return $this->wasMissed;
    }

    /**
     * @param bool $missed
     * @return Penalty
     */
    public function setMissed(bool $missed): Penalty
    {
        $this->wasMissed = $missed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isShootOut(): bool
    {
        return $this->isShootOut;
    }

    /**
     * @param bool $isShootOut
     * @return Penalty
     */
    public function setShootOut(bool $isShootOut): Penalty
    {
        $this->isShootOut = $isShootOut;

        return $this;
    }
}