<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 15/03/2017, 20:18
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Goal.php
 */
namespace SportMonks\Entity\Event;

use SportMonks\Entity\Event;
use SportMonks\Entity\Player;


/**
 * Class Goal
 * @package SportMonks\Entities\Events
 */
class Goal extends Event
{
    /**
     * @var bool
     */
    protected $ownGoal;

    /**
     * @var bool
     */
    protected $disallowed;

    /**
     * @var integer
     */
    protected $playerId;

    /**
     * @var integer
     */
    protected $assistPlayerId;

    /**
     * Goal constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->ownGoal = false;
        $this->disallowed = false;
        parent::__construct($id);
    }

    /**
     * @return bool
     */
    public function isOwnGoal() : bool
    {
        return $this->ownGoal;
    }

    /**
     * @param $ownGoal
     * @return $this
     */
    public function setOwnGoal($ownGoal) : Goal
    {
        $this->ownGoal = $ownGoal;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDisallowed() : bool
    {
        return $this->disallowed;
    }

    /**
     * @param $disallowed
     * @return $this
     */
    public function setDisallowed($disallowed) : Goal
    {
        $this->disallowed = $disallowed;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlayerId(): ?int
    {
        return $this->playerId;
    }

    /**
     * @param int $playerId
     * @return Goal
     */
    public function setPlayerId($playerId): Goal
    {
        if (!\in_array($playerId, Player::$UNKNOWN, false)) {
            $this->playerId = $playerId;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getAssistPlayerId(): ?int
    {
        return $this->assistPlayerId;
    }

    /**
     * @param int $assistPlayerId
     * @return Goal
     */
    public function setAssistPlayerId($assistPlayerId): Goal
    {
        if (!\in_array($assistPlayerId, Player::$UNKNOWN, false)) {
            $this->assistPlayerId = $assistPlayerId;
        }

        return $this;
    }


}
