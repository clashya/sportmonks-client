<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 15/03/2017, 20:21
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Card.php
 */

namespace SportMonks\Entity\Event;

use SportMonks\Entity\Event;
use SportMonks\Entity\Player;

class Card extends Event
{
    public static $YELLOW = 0;
    public static $YELLOW_RED = 1;
    public static $RED = 2;

    /**
     * @var array
     */
    protected static $uniqueAttributes = ['fixtureId', 'teamId', 'minute', 'minuteExtra', 'playerId'];

    /**
     * @var integer
     */
    protected $playerId;

    /**
     * @var integer
     */
    protected $type;

    /**
     * Card constructor.
     * @param int $id
     * @param int $type
     */
    public function __construct($id, $type) {
        parent::__construct($id);
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getPlayerId(): ?int
    {
        return $this->playerId;
    }

    /**
     * @param int $playerId
     * @return Card
     */
    public function setPlayerId($playerId): Card
    {
        if (!\in_array($playerId, Player::$UNKNOWN, false)) {
            $this->playerId = $playerId;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }
}