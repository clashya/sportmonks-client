<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 28/02/2016, 23:03
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Fixture.phphp
 */

namespace SportMonks\Entity;

use Carbon\Carbon;


/**
 * Class Fixture
 * @package SportMonks\Entities
 */
class Fixture
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var \DateTime
     */
    private $date;
    /**
     * @var string
     */
    private $status;
    /**
     * @var integer
     */
    private $homeTeamId;
    /**
     * @var integer
     */
    private $awayTeamId;
    /**
     * @var \stdClass
     */
    private $scores;
    /**
     * @var bool
     */
    private $isKnockout;
    /**
     * @var Event[]
     */
    private $events;
    /**
     * @var int|null
     */
    private ?int $minute;
    /**
     * @var int|null
     */
    private ?int $extraMinute;

    /**
     * Fixture constructor.
     * @param integer $id
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->events = [];
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Fixture
     */
    public function setDate($date): Fixture
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinute(): ?int
    {
        return $this->minute;
    }

    /**
     * @param int|null $minute
     * @return Fixture
     */
    public function setMinute(?int $minute): Fixture
    {
        $this->minute = $minute;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getExtraMinute(): ?int
    {
        return $this->extraMinute;
    }

    /**
     * @param int|null $extraMinute
     * @return Fixture
     */
    public function setExtraMinute(?int $extraMinute): Fixture
    {
        $this->extraMinute = $extraMinute;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Fixture
     */
    public function setStatus($status): Fixture
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return integer
     */
    public function getHomeTeamId(): int
    {
        return $this->homeTeamId;
    }

    /**
     * @param integer $homeTeamId
     * @return Fixture
     */
    public function setHomeTeamId($homeTeamId): Fixture
    {
        $this->homeTeamId = $homeTeamId;

        return $this;
    }

    /**
     * @return integer
     */
    public function getAwayTeamId(): int
    {
        return $this->awayTeamId;
    }

    /**
     * @param integer $awayTeamId
     * @return Fixture
     */
    public function setAwayTeamId($awayTeamId): Fixture
    {
        $this->awayTeamId = $awayTeamId;

        return $this;
    }

    /**
     * @return \stdClass
     */
    public function getScores(): \stdClass
    {
        return $this->scores;
    }

    /**
     * @param \stdClass $scores
     * @return Fixture
     */
    public function setScores($scores): Fixture
    {
        $this->scores = $scores;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsKnockout(): bool
    {
        return $this->isKnockout;
    }

    /**
     * @param bool $isKnockout
     * @return Fixture
     */
    public function setIsKnockout($isKnockout): Fixture
    {
        $this->isKnockout = $isKnockout;

        return $this;
    }

    /**
     * @return Event[]
     */
    public function getEvents(): array
    {
        return $this->events;
    }

    /**
     * @param Event[] $events
     * @return Fixture
     */
    public function setEvents($events): Fixture
    {
        $this->events = $events;

        return $this;
    }

    /**
     * @param integer $id
     * @param \stdClass $fixtureObj
     * @return Fixture
     */
    public static function fromJson($id, $fixtureObj): Fixture
    {
        $fixture = new self($id);

        $fixture
            ->setDate(
                Carbon::createFromTimestamp(
                    $fixtureObj->time->starting_at->timestamp,
                    $fixtureObj->time->starting_at->timezone
                )
            )
            ->setMinute($fixtureObj->time->minute)
            ->setExtraMinute($fixtureObj->time->extra_minute)
            ->setStatus($fixtureObj->time->status)
            ->setHomeTeamId($fixtureObj->localteam_id)
            ->setAwayTeamId($fixtureObj->visitorteam_id)
            ->setScores($fixtureObj->scores)
            ->setIsKnockout($fixtureObj->stage->data->has_standings == false);

        if (isset($fixtureObj->events)) {
            /** @var \stdClass[] $eventsObj */
            $eventsObj = $fixtureObj->events->data;
            usort($eventsObj, function ($eventA, $eventB) {
                return $eventA->id - $eventB->id;
            });

            /** @var Event[] $events */
            $events = [];
            foreach ($eventsObj as $eventObj) {
                if ($event = Event::fromJson($eventObj->id, $eventObj)) {
                    $events[$event->getUniqueKey()] = $event;
                }
            }
            $fixture->setEvents($events);
        }

        return $fixture;
    }
}
