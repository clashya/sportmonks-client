<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 05/09/2016, 22:58
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file Season.php
 */

namespace SportMonks\Entity;

/**
 * Class Season
 * @package SportMonks\Entities
 */
class Season
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * Season constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Season
     */
    public function setName($name): Season
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @param integer $id
     * @param \stdClass $seasonJson
     * @return Season
     */
    public static function fromJson($id, $seasonJson): Season
    {
        $season = new self($id);
        return $season->setName($seasonJson->name);
    }
}