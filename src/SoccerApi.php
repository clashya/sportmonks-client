<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 11/03/2018, 12:58
 *
 * @author Paulo Dias <pdias@gmail.com>
 * @file SoccerApi.php
 */

namespace SportMonks;

use GuzzleHttp\Client as HTTPClient;
use SportMonks\Request\Bookmaker;
use SportMonks\Request\Commentary;
use SportMonks\Request\Continent;
use SportMonks\Request\Country;
use SportMonks\Request\Fixture;
use SportMonks\Request\Head2Head;
use SportMonks\Request\League;
use SportMonks\Request\LiveScore;
use SportMonks\Request\Odds;
use SportMonks\Request\Player;
use SportMonks\Request\Round;
use SportMonks\Request\Season;
use SportMonks\Request\Squad;
use SportMonks\Request\Standings;
use SportMonks\Request\Team;
use SportMonks\Request\TopScorer;
use SportMonks\Request\TVStation;
use SportMonks\Request\Venue;
use SportMonks\Request\Video;

/**
 * Class SoccerApi
 * @package SportMonks
 */
class SoccerApi
{

    /**
     * @var array
     */
    private $config;

    /**
     * SoccerApi constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return Bookmaker
     * @throws \InvalidArgumentException
     */
    public function bookmakers(): Bookmaker
    {
        return new Bookmaker($this->config);
    }

    /**
     * @return Commentary
     * @throws \InvalidArgumentException
     */
    public function commentaries(): Commentary
    {
        return new Commentary($this->config);
    }

    /**
     * @return League
     * @throws \InvalidArgumentException
     */
    public function leagues(): League
    {
        return new League($this->config);
    }

    /**
     * @return Head2Head
     * @throws \InvalidArgumentException
     */
    public function head2head(): Head2Head
    {
        return new Head2Head($this->config);
    }

    /**
     * @return Continent
     * @throws \InvalidArgumentException
     */
    public function continents(): Continent
    {
        return new Continent($this->config);
    }

    /**
     * @return Country
     * @throws \InvalidArgumentException
     */
    public function countries(): Country
    {
        return new Country($this->config);
    }

    /**
     * @return Fixture
     * @throws \InvalidArgumentException
     */
    public function fixtures(): Fixture
    {
        return new Fixture($this->config);
    }

    /**
     * @return TVStation
     * @throws \InvalidArgumentException
     */
    public function tvstations(): TVStation
    {
        return new TVStation($this->config);
    }

    /**
     * @return Venue
     * @throws \InvalidArgumentException
     */
    public function venues(): Venue
    {
        return new Venue($this->config);
    }

    /**
     * @return Round
     * @throws \InvalidArgumentException
     */
    public function rounds(): Round
    {
        return new Round($this->config);
    }

    /**
     * @return LiveScore
     * @throws \InvalidArgumentException
     */
    public function livescores(): LiveScore
    {
        return new LiveScore($this->config);
    }

    /**
     * @return Odds
     * @throws \InvalidArgumentException
     */
    public function odds(): Odds
    {
        return new Odds($this->config);
    }

    /**
     * @return Player
     * @throws \InvalidArgumentException
     */
    public function players(): Player
    {
        return new Player($this->config);
    }

    /**
     * @return Season
     * @throws \InvalidArgumentException
     */
    public function seasons(): Season
    {
        return new Season($this->config);
    }

    /**
     * @return Standings
     * @throws \InvalidArgumentException
     */
    public function standings(): Standings
    {
        return new Standings($this->config);
    }

    /**
     * @return Team
     * @throws \InvalidArgumentException
     */
    public function teams(): Team
    {
        return new Team($this->config);
    }

    /**
     * @return TopScorer
     * @throws \InvalidArgumentException
     */
    public function topscorers(): TopScorer
    {
        return new TopScorer($this->config);
    }

    /**
     * @return Video
     * @throws \InvalidArgumentException
     */
    public function videos(): Video
    {
        return new Video($this->config);
    }

    /**
     * @return Squad
     * @throws \InvalidArgumentException
     */
    public function squads(): Squad
    {
        return new Squad($this->config);
    }
}