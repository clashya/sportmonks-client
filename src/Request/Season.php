<?php

namespace SportMonks\Request;

use SportMonks\Client;
use SportMonks\Entity\Season as SeasonEntity;

class Season extends Client {

    /**
     * @return SeasonEntity[]
     */
    public function all(): array
    {
        return array_map(function ($season) {
            return SeasonEntity::fromJson($season->id, $season);
        }, $this->get('seasons'));
    }

    /**
     * @param $seasonId
     * @return SeasonEntity
     */
    public function byId($seasonId): SeasonEntity
    {
        return SeasonEntity::fromJson($seasonId, $this->get('seasons/' . $seasonId));
    }
}