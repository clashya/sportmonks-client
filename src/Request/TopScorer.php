<?php

namespace SportMonks\Request;

use SportMonks\Client;

class TopScorer extends Client {

    public function bySeasonId($seasonId)
    {
        return $this->get('topscorers/season/' . $seasonId);
    }

}