<?php

namespace SportMonks\Request;


use SportMonks\Client;
use SportMonks\Entity\Country as CountryEntity;

class Country extends Client {

    /**
     * @return CountryEntity[]
     */
    public function all(): array
    {
        return array_map(function ($country) {
            return CountryEntity::fromJson($country->id, $country);
        }, $this->get('countries'));
    }

    /**
     * @param $countryId
     * @return CountryEntity
     */
    public function byId($countryId): CountryEntity
    {
        return CountryEntity::fromJson($countryId, $this->get('countries/' . $countryId));
    }

}