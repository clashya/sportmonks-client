<?php

namespace SportMonks\Request;

use SportMonks\Client;

class Bookmaker extends Client
{

    public function all()
    {
        return $this->get('bookmakers');
    }

}