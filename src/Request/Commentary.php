<?php

namespace SportMonks\Request;

use SportMonks\Client;

class Commentary extends Client {

    public function byMatchId($matchId)
    {
        return $this->get('commentaries/fixture/' . $matchId);
    }

}