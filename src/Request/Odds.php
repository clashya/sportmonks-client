<?php

namespace SportMonks\Request;

use SportMonks\Client;

class Odds extends Client {

    public function byMatchId($matchId)
    {
        return $this->get('odds/fixture/' . $matchId);
    }

    public function byMatchAndBookmakerId($matchId, $bookmakerId)
    {
        return $this->get('odds/fixture/' . $matchId . '/bookmaker/' . $bookmakerId);
    }

    public function inplayByMatchId($matchId)
    {

        return $this->get('odds/inplay/fixture/' . $matchId );
    }

}