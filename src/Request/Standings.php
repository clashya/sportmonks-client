<?php

namespace SportMonks\Request;

use SportMonks\Client;

class Standings extends Client {

    public function bySeasonId($seasonId)
    {
        return $this->get('standings/season/' . $seasonId);
    }

}