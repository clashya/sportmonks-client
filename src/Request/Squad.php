<?php

namespace SportMonks\Request;

use SportMonks\Client;
use SportMonks\Entity\Player;

class Squad extends Client
{

    /**
     * @param $teamId
     * @return Player[]
     */
    public function byTeam($teamId): array
    {
        return array_map(function ($player) {
            $player->player->data->shirt_number = $player->number;
            return Player::fromJson($player->player_id, $player->player->data);
        }, array_filter($this->including('squad.player')->get("teams/{$teamId}")->squad->data, function ($player) {
            return !empty($player->player);
        }));
    }

    /**
     * @param $teamId
     * @param $seasonId
     * @return Player[]
     */
    public function byTeamAndSeason($teamId, $seasonId): array
    {
        return array_map(function ($player) {
            return Player::fromJson($player->player_id, $player->player->data);
        }, $this->get("squad/season/{$seasonId}/team/{$teamId}"));
    }

}
