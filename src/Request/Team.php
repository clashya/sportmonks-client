<?php

namespace SportMonks\Request;

use SportMonks\Client;
use SportMonks\Entity\Team as TeamEntity;

class Team extends Client {

    /**
     * @param $seasonId
     * @return TeamEntity[]
     */
    public function allBySeasonId($seasonId): array
    {
        return array_map(function ($team) {
            return TeamEntity::fromJson($team->id, $team);
        }, $this->get('teams/season/' . $seasonId));
    }

    /**
     * @param $teamId
     * @return TeamEntity
     */
    public function byId($teamId): TeamEntity
    {
        return TeamEntity::fromJson($teamId, $this->get('teams/' . $teamId));
    }

}