<?php

namespace SportMonks\Request;


use SportMonks\Client;

class Continent extends Client {

    public function all()
    {
        return $this->get('continents/');
    }

    public function byId($continentId)
    {
        return $this->get('continents/' . $continentId);
    }

}