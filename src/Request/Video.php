<?php

namespace SportMonks\Request;

use SportMonks\Client;

class Video extends Client {

    public function all()
    {
        return $this->get('highlights/');
    }

    public function byMatchId($matchId)
    {
        return $this->get('highlights/fixture/' . $matchId);
    }

}