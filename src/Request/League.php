<?php

namespace SportMonks\Request;

use SportMonks\Client;
use SportMonks\Entity\League as LeagueEntity;

class League extends Client {

    /**
     * @return LeagueEntity[]
     */
    public function all(): array
    {
        return array_map(function ($league) {
            return LeagueEntity::fromJson($league->id, $league);
        }, $this->including('country')->including('season')->get('leagues'));
    }

    /**
     * @param $leagueId
     * @return LeagueEntity
     */
    public function byId($leagueId): LeagueEntity
    {
        return LeagueEntity::fromJson($leagueId, $this->get('leagues/' . $leagueId));
    }

}