<?php

namespace SportMonks\Request;

use SportMonks\Client;
use SportMonks\Entity\Player as PlayerEntity;

class Player extends Client {

    /**
     * @param $playerId
     * @return PlayerEntity
     * @throws \InvalidArgumentException
     */
    public function byId($playerId): PlayerEntity
    {
        return PlayerEntity::fromJson($playerId, $this->get('players/' . $playerId));
    }

}