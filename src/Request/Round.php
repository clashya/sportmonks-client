<?php

namespace SportMonks\Request;


use SportMonks\Client;

class Round extends Client {

    public function byId($roundId)
    {
        return $this->get('rounds/' . $roundId);
    }

    public function bySeasonId($seasonId)
    {
        return $this->get('rounds/season/' . $seasonId);
    }

}