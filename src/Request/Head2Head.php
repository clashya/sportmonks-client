<?php

namespace SportMonks\Request;

use SportMonks\Client;

class Head2Head extends Client {

    public function betweenTeams($team1Id, $team2Id)
    {
        return $this->get('head2head/' . $team1Id . '/' . $team2Id);
    }

}