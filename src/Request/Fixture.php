<?php

namespace SportMonks\Request;

use Carbon\Carbon;
use SportMonks\Client;
use SportMonks\Entity\Fixture as FixtureEntity;

class Fixture extends Client {

    /**
     * @param int $seasonId
     * @return FixtureEntity[]
     */
    public function bySeason(int $seasonId): array
    {
        return array_map(function ($fixture) {
            return FixtureEntity::fromJson($fixture->id, $fixture);
        }, $this->including('fixtures.events')->including('fixtures.stage')->get("seasons/{$seasonId}")->fixtures->data);
    }

    /**
     * @param Carbon $fromDate
     * @param Carbon $toDate
     * @return FixtureEntity[]
     */
    public function betweenDates(Carbon $fromDate, Carbon $toDate): array
    {
        return array_map(function ($fixture) {
            return FixtureEntity::fromJson($fixture->id, $fixture);
        }, $this->get('fixtures/between/' . $fromDate->format('Y-m-d') . '/' .$toDate->format('Y-m-d')));
    }

    /**
     * @param Carbon $date
     * @return FixtureEntity[]
     */
    public function byDate(Carbon $date): array
    {
        return array_map(function ($fixture) {
            return FixtureEntity::fromJson($fixture->id, $fixture);
        }, $this->get('fixtures/date/' . $date->format('Y-m-d')));
    }

    /**
     * @param int $id
     * @return FixtureEntity
     */
    public function byFixtureId(int $id): FixtureEntity
    {
        return FixtureEntity::fromJson($id, $this->get('fixtures/' . $id));
    }
}