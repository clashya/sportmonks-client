<?php

namespace SportMonks\Request;


use SportMonks\Client;

class Venue extends Client {

    public function byId($venueId)
    {
        return $this->get('venues/' . $venueId);
    }

}