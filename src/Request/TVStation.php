<?php

namespace SportMonks\Request;

use SportMonks\Client;

class TVStation extends Client {

    public function byMatchId($id)
    {
        return $this->get('tvstations/fixture/' . $id);
    }
}