<?php

namespace SportMonks\Request;

use SportMonks\Client;
use SportMonks\Entity\Fixture;

class LiveScore extends Client {

    /**
     * @return Fixture[]
     */
    public function today(): array
    {
        return array_map(function ($fixture) {
            return Fixture::fromJson($fixture->id, $fixture);
        }, $this->get('livescores'));
    }

    /**
     * @return Fixture[]
     */
    public function now(): array
    {
        return array_map(function ($fixture) {
            return Fixture::fromJson($fixture->id, $fixture);
        }, $this->get('livescores/now'));
    }

}