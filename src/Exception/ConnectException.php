<?php
/**
 * This file is part of the sportmonks-client package.
 * Created at 05/09/2016, 22:58
 *
 * @author Paulo Dias <prbdias@gmail.com>
 * @file ConnectException.php
 */
namespace SportMonks\Exception;


class ConnectException extends \Exception
{
}